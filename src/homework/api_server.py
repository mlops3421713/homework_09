import asyncio

from fastapi import FastAPI
from pydantic import BaseModel, ConfigDict
from celery.result import AsyncResult
from .task_server import sam_task

app = FastAPI()


class TextRequest(BaseModel):
    """
    The expected request body.
    """

    text: str
    lang: str | None = "ru"

    model_config = ConfigDict(
        json_schema_extra={
            "example": {
                "text": "Does the black moon howl?",
                "lang": "en",
            }
        }
    )


@app.post("/predict/")
async def predict_emotion(text_request: TextRequest):
    """
    Sentiment analysis endpoint processing

    Args:
        text_request (TextRequest): a request to process

    Returns:
        dict: classification result (expected to contain 'label' and 'score' keys);

    """
    print("running endpoint")
    task = sam_task.delay(
        text_request.dict().get("text"), text_request.dict().get("lang")
    )

    while not task.ready():
        await asyncio.sleep(0.1)

    result = AsyncResult(task.id).result

    return result
