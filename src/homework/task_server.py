import os

from celery import Celery
from kombu import Queue
from .model import EmotionClassifier


redis_server = os.environ.get("REDIS_SERVER", "redis://localhost:6379/0")
rabbitmq_server = os.environ.get(
    "RABBITMQ_SERVER", "pyamqp://guest:guest@localhost:5672//"
)

# QA: using RabbitMQ for the sake of using RabbitMQ is a tech requirement.
# Redis can take both broker and backend jobs just fine.
celery_app = Celery(
    "emotion_classifier",
    # broker=redis_server,
    broker=rabbitmq_server,
    result_backend=redis_server,
    worker_timer_precision=0.1,
)

# Extendable to more queues with different rules.
celery_app.conf.task_queues = (Queue("sam", routing_key="sam.#"),)


@celery_app.task(queue="sam")
def sam_task(text, lang):
    """
    Sentiment analysis task. Creates a one-shot pipeline, reasonably fast and scalable.

    Args:
        text (str): text to classify
        lang (str): whether to use the 'en' or 'ru' model
    Returns:
        dict: classification result (expected to contain 'label' and 'score' keys)
    """
    sentiment = EmotionClassifier.predict_emotion(text, lang)
    return sentiment
