import torch
from transformers import pipeline

torch.set_num_threads(1)


class EmotionClassifier:
    """
    Sentiment analysis with a pretrained HF model.
    """

    model_ru = pipeline(
        "text-classification", model="seara/rubert-tiny2-ru-go-emotions"
    )
    model_en = pipeline(
        "text-classification",
        model="j-hartmann/emotion-english-distilroberta-base",
    )

    @classmethod
    def predict_emotion(cls, text, lang="ru"):
        """
        Classify the text.

        Args:
            text (str): text for the inference
            lang (str): whether to use the 'en' or 'ru' model (default and fallback: 'ru')

        Returns:
            dict: classification result (expected to contain 'label' and 'score' keys)
        """
        result = cls.model_en(text) if (lang == "en") else cls.model_ru(text)
        return result
