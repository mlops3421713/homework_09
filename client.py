"""
Clients to the API server. Currently includes
APIClient() - base client, and its descendant:
SAMClient() - for sentiment analysis.
"""

import requests
from time import sleep


class APIClient:
    def __init__(self, url):
        """
        Initialize the base API client.

        Args:
            url (str): an URL of the API server
        """
        self.url = url

    def request(self, path, data, stream=False):
        """
        Basic API POST request to self.url + path.

        Args:
            path (str): API endpoint path (e.g.: '/api/method')
            data (dict): JSON-like payload

        Returns:
            requests.Response: the response
        """
        status = 500
        attempts = 0
        # TODO: handle a finite number of retries.
        while status != 200:
            if attempts > 1:
                print(
                    f"Failed to fetch data, status code {response.status_code}. Retrying..."  # noqa: F821
                )
                sleep(5)
            response = requests.post(
                f"{self.url}{path}", json=data, stream=stream
            )
            status = response.status_code
            attempts += 1
        return response


class SAMClient(APIClient):
    def __init__(self, url="http://localhost:5009"):
        """
        Initialize the API client for sentiment analysis.

        Args:
            url (str): an URL of the API server, default assumption is running locally on a port 5009
        """
        super().__init__(url)

    def classify_emotion(self, text, lang="ru"):
        """
        Classify a text string.

        Args:
            text (str): text to classify
            lang (str): whether to use the 'en' or 'ru' model (default: 'ru')

        Returns:
            dict: expected to contain 'label' and 'score' keys
        """
        data = {
            "text": text,
            "lang": lang,
        }

        response = self.request("/predict/", data)
        return response.json()[0]
