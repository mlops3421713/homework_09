# MLops homework: Web application using Celery, FastAPI, Redis, RabbitMQ and Streamlit

This is a homework repository for the [MLOps & production for data science research 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

A `homework` module implements the task and API servers. The client and the frontend code are provided at the top level.

Docs up at https://homework-09-mlops3421713-28d4b06d3b125541b91268c18166eb6882903f.gitlab.io/

## Running locally:

`conda env create -f web.yaml`

`conda run -n web pdm install`

Environment variables `REDIS_SERVER` and `RABBITMQ_SERVER` are expected to specify the Redis and RabbitMQ URLs. If none, will try to access the default ports on localhost.

A shell script to start/restart/stop the task queues (extendable to more queues in the future):

`conda run -n web ./launch.sh start`

Running the frontend:

`conda run -n web streamlit run frontend.py --server.port 13666`

REST API is also available at the port 5009.


## Running in a container:

`docker compose up`

Wait for the services to come online and and access `localhost:13666` with a browser.


## Things to consider:

Both backend and frontend run in the same container. At the present moment, the overhead would exceed any benefits of separation, but it might change in the future.

Logs are output into `api_server.log` and `sam.log`. Might be worth making a docker volume.
