#!/bin/bash

limited_queues=()
unlimited_queues=("sam")

case "$1" in
  start)
    for queue in "${limited_queues[@]}"; do
      celery -A homework.task_server.celery_app multi start worker --loglevel=info -n $queue-%n -Q $queue -c 1 --pidfile="/tmp/celery/$queue.pid" --logfile="$queue.log" &
    done
    for queue in "${unlimited_queues[@]}"; do
      celery -A homework.task_server.celery_app multi start worker --loglevel=info -n $queue-%n -Q $queue -c 4 --pidfile="/tmp/celery/$queue.pid" --logfile="$queue.log" &
    done
    uvicorn homework.api_server:app --port 5009 --reload > api_server.log 2>&1 &
    ;;
  restart)
    for queue in "${limited_queues[@]}"; do
      celery -A homework.task_server.celery_app multi restart worker --loglevel=info -n $queue-%n -Q $queue -c 1 --pidfile="/tmp/celery/$queue.pid" --logfile="$queue.log" &
    done
    for queue in "${unlimited_queues[@]}"; do
      celery -A homework.task_server.celery_app multi restart worker --loglevel=info -n $queue-%n -Q $queue -c 4 --pidfile="/tmp/celery/$queue.pid" --logfile="$queue.log" &
    done
    wait
    ;;
  stop)
    for queue in "${limited_queues[@]}" "${unlimited_queues[@]}"; do
      celery multi stopwait worker --pidfile="/tmp/celery/$queue.pid" --logfile="$queue.log" &
    done
    killall uvicorn
    wait
    ;;
  *)
    echo "Usage: $0 {start|restart|stop}"
    exit 1
    ;;
esac
