import streamlit as st

from client import SAMClient


def init_page():
    st.set_page_config(
        page_title="SAM Frontend",
        page_icon="🤗",
        layout="wide",
    )

    st.sidebar.title("Settings")

    st.session_state.setdefault("model", "rubert-tiny2-ru")
    st.session_state.setdefault("sam_client", SAMClient())


def sam_page():
    selection = st.sidebar.radio(
        "Model language",
        ["Russian", "English"],
        captions=["rubert-tiny2-ru", "distilroberta-base"],
    )

    text = st.text_area(
        "Your text:", value="Does the black moon howl?", max_chars=512
    )

    if st.button("Classify"):
        if text:
            result = st.session_state.sam_client.classify_emotion(
                text,
                "ru" if selection == "Russian" else "en",
            )
            label = result.get("label")
            score = result.get("score")
            report = (
                f"With {score:.0%} certainty this text expresses **{label}**"
            )
            st.write(report)


def main():
    init_page()
    sam_page()


if __name__ == "__main__":
    main()
