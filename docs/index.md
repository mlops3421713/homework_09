# Sentiment analysis with Transformers using Streamlit, FastAPI, Celery, Redis and RabbitMQ

There's only one API endpoint currently (`/predict/`).

POST payload example:

```
    {
        "text": "Does the black moon howl?",
        "lang": "en",
    }
```

Returns a dict with `label` and `score` fields.
