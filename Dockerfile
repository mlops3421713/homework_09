FROM continuumio/miniconda3

WORKDIR /app

# Creating environment
COPY web.yaml .
RUN conda env create -f web.yaml

# Unit activation
COPY . .
RUN conda run -n web pdm install
RUN chmod +x launch.sh
